# Project's main package

## LICENSING

This software is protected by the same license as the [main Cyborg repository](https://github.com/cyborg/cyborg). You can find the license file [here](https://github.com/cyborg/cyborg/blob/master/LICENSE).
